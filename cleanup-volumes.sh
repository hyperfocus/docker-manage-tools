#!/bin/bash

# List dangling volumes
docker volume ls -qf dangling=true

# Remove dangling volumes
docker volume rm $(docker volume ls -qf dangling=true)
